<?php

	if(isset($_POST["submit"])){
// Checking For Blank Fields..
		if($_POST["name"]==""||$_POST["email"]==""){
			echo "Fill All Fields..";
		}else{
// Check if the "Sender's Email" input field is filled out
			$email=$_POST['email'];
			// Sanitize E-mail Address
			$email =filter_var($email, FILTER_SANITIZE_EMAIL);
			// Validate E-mail Address
			$email= filter_var($email, FILTER_VALIDATE_EMAIL);
			
				if (!$email){
					echo "Invalid Sender's Email";
				}
				
				else{
						$subject = "Test Email";
						$message = "Hi ".$_POST["name"].",";
						$message .= "<br/><br/>This Message is from T3 Education. Thanks for your interest. We will get back to you shortly!";
						$message .="<br/><br/>Best Regards,<br/>Saurabh Jain";
						
						$headers = "MIME-Version: 1.0" . "\r\n";
						$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
						$headers .= 'From:'.'Saurabh Jain <saurabh.jain@nyu.edu>'. "\r\n"; // Sender's Email
						//$headers .= 'Cc:'. 'saurabh.jain@nyu.edu' . "\r\n"; // Carbon copy to Sender
						// Message lines should not exceed 70 characters (PHP rule), so wrap it
						//$message = wordwrap($message, 70);
			// Send Mail By PHP Mail Function
						mail($email, $subject, $message, $headers);
						echo "Your mail has been sent successfuly ! Thank you for your interest";
						header('Location: '.$_POST["page"]);
						exit;
					}
			}
		}
		

		//echo "works";
?>